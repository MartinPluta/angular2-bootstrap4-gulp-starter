# Applied Technologies

* AngularJS 2
* Bootstrap 4
* Font Awesome
* Gulp (as a task runner)
* Lite Server
* BrowserSync
* TSLint

# Install Instructions

* Install [Node.js](https://nodejs.org)
* Clone this repository
* Change to root directory of the application
* Run the following commands: 
```
#!shell

npm install
```
```
#!shell

npm start
```

A Browser should open and display the website. Gulp watches the files in the 'src' directory for changes. If one or more files change, gulp will copy them to the 'dist' folder and refresh the website automatically.


```
#!shell

To stop the Server from running, press: 'STRG + C'

```


```
#!shell

To clean the dist directory, run: 'npm clean-distribution'
```

 