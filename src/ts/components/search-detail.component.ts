import { Component, OnInit } from '@angular/core';
import { Image } from '../model/image';
import { SearchService } from '../services/search.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'search-detail',
    templateUrl: '../../html/search-detail.component.html',
    styleUrls: ['../../css/search-detail.component.css']
})

export class SearchDetailComponent implements OnInit {
    private image: Image;
    public showDetail: boolean;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private searchService: SearchService
    ) {}

    ngOnInit(): void {
      this.showDetail = true;
      this.route.params.forEach((params: Params) => {
        let id = params['id'];
        this.getImages(id);
      });
    }

    getImages(id: string): void {
      this.searchService.getImage(id)
            .subscribe(res => this.image = res);
    }

}
