import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'my-app',
    templateUrl: '../../html/app.component.html',
    styleUrls: ['../../css/app.component.css']
})
export class AppComponent {
  public isCollapsed: boolean = true;
  public searchTerm = '';
  constructor(private route: ActivatedRoute,
              private router: Router,
  ) {}
  onSubmit() {
     this.router.navigate(['/search', this.searchTerm]);
   }

   public isRoute(route: string): boolean {
     return (this.router.url === route);
   }
}
