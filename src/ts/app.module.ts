import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppComponent }  from './components/app.component';
import { FrontpageComponent }  from './components/frontpage.component';
import { SearchResultComponent }  from './components/search-result.component';
import { SearchDetailComponent }  from './components/search-detail.component';

import { SearchService } from './services/search.service';

@NgModule({
  imports: [
    NgbModule.forRoot(),
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      {
        path: '',
        component: FrontpageComponent
      },
      {
        path: 'search/:searchTerm',
        component: SearchResultComponent
      },
      {
        path: 'detail/:id',
        component: SearchDetailComponent
      }
    ])
   ],
  declarations: [
    AppComponent,
    FrontpageComponent,
    SearchResultComponent,
    SearchDetailComponent
    ],
  providers: [
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    SearchService
  ],
  bootstrap: [ AppComponent ],
})
export class AppModule { }
