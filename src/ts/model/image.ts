export class Image {
    id: string;
    title: string;
    caption: string;
    previewUri: string;
    artist: string;
    width: number;
    height: number;
    copyright: string;
    usageRestrictions: string;
    dateCreated: string;
    dateSubmitted: string;
    sourceUri: string;

    constructor(id: string,
                title: string,
                caption: string,
                previewUri: string,
                artist?: string,
                width?: number,
                height?: number,
                copyright?: string,
                usageRestrictions?: string,
                dateCreated?: string,
                dateSubmitted?: string,
                sourceUri?: string) {
      this.id = id;
      this.title = title;
      this.caption = caption;
      this.previewUri = previewUri;
      this.artist = artist;
        if (typeof width === 'number') {
          this.width = width;
        }
        if (typeof height === 'number') {
          this.height = height;
        }
        if (typeof copyright === 'string') {
          this.copyright = copyright;
        }
        if (typeof usageRestrictions === 'string') {
          this.usageRestrictions = usageRestrictions;
        }
        if (typeof dateCreated === 'string') {
          this.dateCreated = dateCreated;
        }
        if (typeof dateSubmitted === 'string') {
          this.dateSubmitted = dateSubmitted;
        }
        if (typeof sourceUri === 'string') {
          this.sourceUri = sourceUri;
        }
    }
}
