import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Image } from '../model/image';

@Injectable()
export class SearchService {
  private apiKey = 'ky4y8pcmfzg4dw23vb9n9tgw';
  private headers = new Headers({ 'Api-Key': this.apiKey, 'Accept-Language': 'en-US' });

  constructor(private http: Http) {}

  public getImages(searchTerm: string): Observable<Image[]> {
    let apiUrl = 'https://api.gettyimages.com/v3/search/images?phrase=';
    apiUrl += searchTerm;
    let options = new RequestOptions({ headers: this.headers });
    return this.http.get(apiUrl, options)
                         .map( (responseData) => {
                            return responseData.json();
                          })
                          .map((data: Array<any>) => {
                            return this.getImageArrayFromResponse(data);
                          });
  }

  private getImageArrayFromResponse(data: Array<any>) {
      let images: Array<Image> = [];
      if (data['images']) {
        data['images'].forEach((source: any) => {
          images.push(
                    new Image(source.id,
                              source.title,
                              source.caption,
                              source.display_sizes[0].uri));
        });
      }
      return images;
  }

  public getImage(id: string): Observable<Image> {
    let apiUrl = 'https://api.gettyimages.com/v3/images/';
    let uriParams = '?fields=display_set,summary_set,detail_set';
    apiUrl += id + uriParams;
    let options = new RequestOptions({ headers: this.headers });
    return this.http.get(apiUrl, options)
                         .map( (responseData) => {
                            return responseData.json();
                          })
                          .map((data: Array<any>) => {
                            return this.getImageFromResponse(data);
                          });
  }

  private getImageFromResponse(data: Array<any>) {
      let image: Image;
      if (data['images']) {
        data['images'].forEach((source: any) => {
          image = new Image(source.id,
                            source.title,
                            source.caption,
                            source.display_sizes[0].uri,
                            source.artist,
                            source.max_dimensions.width,
                            source.max_dimensions.height,
                            source.copyright,
                            source.allowed_use.usage_restrictions[0],
                            source.date_created,
                            source.date_submitted,
                            source.referral_destinations[0].uri
                            );
        });
      }
      return image;
  }
}
