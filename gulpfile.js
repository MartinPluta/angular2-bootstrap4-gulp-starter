var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var del = require('del');
var ts = require('gulp-typescript');
var sourcemaps  = require('gulp-sourcemaps');
var tslint = require('gulp-tslint');
var tsProject = ts.createProject('tsconfig.json');

var browser = null;

gulp.task('start', ['watch:source', 'build-and-display'], function() {
});

gulp.task('watch:source', function () {
    gulp.watch('src/**/*.*', ['build-and-display']);
});

gulp.task('build-and-display', ['build'], function() {
    displayPage();
});

gulp.task('build', ['code-analysis:tslint', 'compile:ts', 'copy:libs', 'copy:html', 'copy:css', 'copy:assets'], function() {
});

gulp.task('code-analysis:tslint', function() {
    gulp.src('src/ts/**/*.ts')
        .pipe(tslint({
            formatter: 'verbose'
        }))
        .pipe(tslint.report())
});

gulp.task('compile:ts', function() {
    var tsResult = gulp.src('src/ts/**/*.ts')
        .pipe(sourcemaps.init())
        .pipe(tsProject());
    return tsResult.js
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist/js'));
});

gulp.task('copy:libs', function() {
  return gulp.src([
      'node_modules/core-js/client/shim.min.js',
      'node_modules/zone.js/dist/zone.js',
      'node_modules/reflect-metadata/Reflect.js',
      'node_modules/systemjs/dist/system.src.js',
      'node_modules/rxjs/bundles/Rx.js',
      'node_modules/angular2/bundles/angular2.dev.js',
      'node_modules/angular2/bundles/router.dev.js',
      'node_modules/jquery/dist/jquery.min.js',
      'node_modules/tether/dist/js/tether.min.js',
      'systemjs.config.js'
    ])
    .pipe(gulp.dest('dist/lib'))
});

gulp.task('copy:html', function() {
  return gulp.src('src/html/**/*')
    .pipe(gulp.dest('dist/html'))
});

gulp.task('copy:css', function() {
  return gulp.src('src/css/**/*')
    .pipe(gulp.dest('dist/css'))
});

gulp.task('copy:assets', function() {
  return gulp.src('src/assets/**/*')
    .pipe(gulp.dest('dist/assets'))
});

gulp.task('clean:distribution', function () {
  return del('dist/**/*');
});

function displayPage() {
    if (browser) {
        browserSync.reload();
    } else {
        browser = browserSync.init({
            'port': 8000,
            'files': [
                'dist/**/*.{html,htm,css,js}'
            ],
            'server': {
                'baseDir': './'
            }
        });
    }
}
